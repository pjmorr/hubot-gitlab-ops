help = require "../../../scripts/status"

describe "status script", ->
  robot = null

  beforeEach ->
    robot =
      router:
        get: ->
      webui:
        addMenu: ->

  describe "webui", ->
    it "should have the /status route", ->
      spyOn(robot.router, "get");

      help robot

      expect(robot.router.get).toHaveBeenCalledWith "/status",
        jasmine.any Function

    it "should have the Status menu", ->
      spyOn(robot.webui, "addMenu");

      help robot

      expect(robot.webui.addMenu).toHaveBeenCalledWith "Status",
        "/status",
        "check-circle",
        0
