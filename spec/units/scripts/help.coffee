help = require "../../../scripts/help"

describe "help script", ->
  robot = null

  beforeEach ->
    robot =
      respond: ->
      router:
        get: ->
      webui:
        addMenu: ->

  describe "commands", ->
    it "should have the help command", ->
      spyOn(robot, "respond");

      help robot

      expect(robot.respond).toHaveBeenCalledWith /help(?:\s+(.*))?$/i,
        jasmine.any Function

  describe "webui", ->
    it "should have the /help route", ->
      spyOn(robot.router, "get");

      help robot

      expect(robot.router.get).toHaveBeenCalledWith "/help",
        jasmine.any Function

    it "should have the Help menu", ->
      spyOn(robot.webui, "addMenu");

      help robot

      expect(robot.webui.addMenu).toHaveBeenCalledWith "Help",
        "/help",
        "question-circle",
        99
