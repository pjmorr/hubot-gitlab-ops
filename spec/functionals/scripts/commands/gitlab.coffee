Helper = require "hubot-test-helper"
q = require "q"
gitlab =  new Helper "../../../../scripts/gitlab.coffee"
Client =  require "../../../../lib/gitlab/client"

describe "gitlab commands", ->
  beforeEach ->
    @room = gitlab.createRoom()

  afterEach ->
    @room.destroy()

  describe 'user requests authentication', ->
    beforeEach (done) ->
      @room.user.say "jasmine", "hubot gitlab auth"
      @room.user.say "aladin", "hubot gitlab authenticate"

      setTimeout -> done()

    it "should reply with authentication URL", ->
      @room.messages.length.should.equal 4

      @room.messages[0].should.eql ["jasmine", "hubot gitlab auth"]
      @room.messages[1].should.eql ["aladin", "hubot gitlab authenticate"]
      @room.messages[2].should.eql ["hubot", "@jasmine http://192.168.33.10:8075/auth/jasmine"]
      @room.messages[3].should.eql ["hubot", "@aladin http://192.168.33.10:8075/auth/aladin"]

  describe "user requests project issue list", ->
    beforeEach (done) ->
      spyOn(Client, "createClient").and.callThrough()
      spyOn(Client.prototype, "issues").and.returnValue q.when [
        { iid: 4, title: "An issue" },
        { iid: 5, title: "Another issue" },
      ]

      @room.robot.brain.data._private["user:jasmine"] = 'jasminetoken'
      @room.user.say "jasmine", "hubot gitlab issues foo/bar"

      setTimeout -> done()

    it "should reply with project issue list", ->
      @room.messages.length.should.equal 2

      @room.messages[0].should.eql ["jasmine", "hubot gitlab issues foo/bar"]
      @room.messages[1].should.eql ["hubot", "#4 - An issue\nhttp://192.168.33.10/foo/bar/issues/4\n\n#5 - Another issue\nhttp://192.168.33.10/foo/bar/issues/5\n\n"]

      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "jasminetoken"
      expect(Client.prototype.issues).toHaveBeenCalledWith "foo", "bar", 5

  describe "user requests a project issue", ->
    beforeEach (done) ->
      spyOn(Client, "createClient").and.callThrough()
      spyOn(Client.prototype, "issue").and.returnValue q.when { iid: 4, title: "An issue" }

      @room.robot.brain.data._private["user:jasmine"] = 'jasminetoken'
      @room.robot.brain.data._private["user:aladin"] = 'aladintoken'
      @room.user.say "jasmine", "hubot gitlab issue foo/bar#4"
      @room.user.say "aladin", "hubot gitlab foo/bar#4"

      setTimeout -> done()

    it "should reply with the project issue summary", ->
      @room.messages.length.should.equal 4

      @room.messages[0].should.eql ["jasmine", "hubot gitlab issue foo/bar#4"]
      @room.messages[1].should.eql ["aladin", "hubot gitlab foo/bar#4"]
      @room.messages[2].should.eql ["hubot", "#4 - An issue\nhttp://192.168.33.10/foo/bar/issues/4\n"]
      @room.messages[3].should.eql ["hubot", "#4 - An issue\nhttp://192.168.33.10/foo/bar/issues/4\n"]

      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "jasminetoken"
      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "aladintoken"
      expect(Client.prototype.issue).toHaveBeenCalledWith "foo", "bar", "4"

  describe "user requests project merge request list", ->
    beforeEach (done) ->
      spyOn(Client, "createClient").and.callThrough()
      spyOn(Client.prototype, "merge_requests").and.returnValue q.when [
        { iid: 4, title: "A merge request" },
        { iid: 5, title: "Another merge request" },
      ]

      @room.robot.brain.data._private["user:jasmine"] = 'jasminetoken'
      @room.robot.brain.data._private["user:aladin"] = 'aladintoken'
      @room.user.say "jasmine", "hubot gitlab merge requests foo/bar"
      @room.user.say "aladin", "hubot gitlab mrs foo/bar"

      setTimeout -> done()

    it "should reply with project merge request list", ->
      @room.messages.length.should.equal 4

      @room.messages[0].should.eql ["jasmine", "hubot gitlab merge requests foo/bar"]
      @room.messages[1].should.eql ["aladin", "hubot gitlab mrs foo/bar"]
      @room.messages[2].should.eql ["hubot", "#4 - A merge request\nhttp://192.168.33.10/foo/bar/merge_requests/4\n\n#5 - Another merge request\nhttp://192.168.33.10/foo/bar/merge_requests/5\n\n"]
      @room.messages[3].should.eql ["hubot", "#4 - A merge request\nhttp://192.168.33.10/foo/bar/merge_requests/4\n\n#5 - Another merge request\nhttp://192.168.33.10/foo/bar/merge_requests/5\n\n"]

      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "jasminetoken"
      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "aladintoken"
      expect(Client.prototype.merge_requests).toHaveBeenCalledWith "foo", "bar", 5

  describe "user requests a project merge request", ->
    beforeEach (done) ->
      spyOn(Client, "createClient").and.callThrough()
      spyOn(Client.prototype, "merge_request").and.returnValue q.when { iid: 4, title: "A merge request" }

      @room.robot.brain.data._private["user:jasmine"] = 'jasminetoken'
      @room.robot.brain.data._private["user:aladin"] = 'aladintoken'
      @room.robot.brain.data._private["user:abu"] = 'abutoken'
      @room.user.say "jasmine", "hubot gitlab merge request foo/bar!4"
      @room.user.say "aladin", "hubot gitlab mr foo/bar!4"
      @room.user.say "abu", "hubot gitlab foo/bar!4"

      setTimeout -> done()

    it "should reply with the project merge request summary", ->
      @room.messages.length.should.equal 6

      @room.messages[0].should.eql ["jasmine", "hubot gitlab merge request foo/bar!4"]
      @room.messages[1].should.eql ["aladin", "hubot gitlab mr foo/bar!4"]
      @room.messages[2].should.eql ["abu", "hubot gitlab foo/bar!4"]
      @room.messages[3].should.eql ["hubot", "#4 - A merge request\nhttp://192.168.33.10/foo/bar/merge_requests/4\n"]
      @room.messages[4].should.eql ["hubot", "#4 - A merge request\nhttp://192.168.33.10/foo/bar/merge_requests/4\n"]
      @room.messages[5].should.eql ["hubot", "#4 - A merge request\nhttp://192.168.33.10/foo/bar/merge_requests/4\n"]

      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "jasminetoken"
      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "aladintoken"
      expect(Client.createClient).toHaveBeenCalledWith "http://192.168.33.10/api/v3", "abutoken"
      expect(Client.prototype.merge_request).toHaveBeenCalledWith "foo", "bar", "4"
