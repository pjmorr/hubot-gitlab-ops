request = require "request-promise"

class Client
  constructor: (@url, @token) ->

  getOptions: (method, path) ->
    {
      uri: "#{@url}#{path}",
      qs: {
        access_token: @token
      },
      headers: {
        'User-Agent': 'Hubot'
      },
      json: true
    };

  user: ->
    request.get(@getOptions("GET", "/user"));

  issues: (namespace, repository, limit) ->
    options = @getOptions("GET", "/projects/#{namespace}%2F#{repository}/issues")
    options.qs.per_page = limit

    request.get(options)

  issue: (namespace, repository, iid) ->
    options = @getOptions("GET", "/projects/#{namespace}%2F#{repository}/issues")
    options.qs.iid = iid

    request.get(options).then (body) -> body[0]

  merge_requests: (namespace, repository, limit) ->
    options = @getOptions("GET", "/projects/#{namespace}%2F#{repository}/merge_requests")
    options.qs.per_page = limit

    request.get(options)

  merge_request: (namespace, repository, iid) ->
    options = @getOptions("GET", "/projects/#{namespace}%2F#{repository}/merge_requests")
    options.qs.iid = iid

    request.get(options).then (body) -> body[0]

  milestones: (namespace, repository, limit) ->
    options = @getOptions("GET", "/projects/#{namespace}%2F#{repository}/milestones")
    options.qs.per_page = limit

    request.get(options)

module.exports = Client

module.exports.createClient = (url, token) ->
  new Client url, token
