fs = require "fs"
mustache = require "mustache"
express = require "express"

merge = (objects...) ->
  if objects?.length > 0
    tap {}, (m) -> m[key] = value for key, value of sub for sub in objects

tap = (o, fn) -> fn(o); o

class WebUI
  constructor: (@robot, stat) ->
    @menu = [];
    @active = null

    @addStatic stat if stat

  addMenu: (label, url, icon, position) ->
    @menu.push label: label, url: url, icon: icon, position: position

    @menu.sort (a, b) -> return a.position - b.position;

  activateMenu: (url) ->
    @active = url

  addStatic: (directory, url) ->
    if url
      @robot.router.use express.static url, directory
    else
      @robot.router.use express.static directory

  render: (template, view) ->
    title = null

    @menu.forEach (menu) =>
      menu.class = null

      if menu.url == @active
        menu.class = "active"
        title = menu.label

    @active = null

    defaults = {
      title: title,
      menu: @menu,
      robot: @robot
    }

    mustache.render fs.readFileSync("#{__dirname}/../views/index.html").toString(),
      merge(defaults, view),
      { content: template }

module.exports = WebUI
