# Description:
#   Generates help commands for Hubot.
#
# Commands:
#   hubot gitlab auth[enticate] - Authenticates a user against gitlab
#   hubot gitlab issues <namespace>/<repository> - Shows last 5 project issues
#   hubot gitlab [issue] <namespace>/<repository>#<iid> - Shows a project issue
#   hubot gitlab (merge requests|mrs) <namespace>/<repository> - Shows last 5 project merge requests
#   hubot gitlab [(merge request|mr)] <namespace>/<repository>!<iid> - Shows a project merge request
#   hubot gitlab milestones <namespace>/<repository> - Shows last 5 project milestone
#
# URLS:
#   /help
#
# Notes:
#   These commands are grabbed from comment blocks at the top of each file.

fs = require "fs"
passport = require "passport"
OAuth2 = require("passport-oauth2").Strategy
Client = require "../lib/gitlab/client"
issueRenderer = require "../lib/renderer/gitlab/issue"
mergeRequestRenderer = require "../lib/renderer/gitlab/merge-request"

module.exports = (robot) ->
  config = {
    authorizationURL: "http://192.168.33.10/oauth/authorize",
    tokenURL: "http://192.168.33.10/oauth/token",
    callbackURL: "http://192.168.33.10:8075/auth/callback",
    clientID: "5c2759e8cbc95cc36d63c2f2b4ce18ec70656e5f3a1cce6d184fe445dbbaa6e2",
    clientSecret: "d75217c092ec943da91963813ed789405e2bee3b8a4a735719885992beef3b53"
  }

  strategy = new OAuth2 config, (accessToken, refreshToken, profile, done) ->
    done(null, accessToken)

  auth = passport.use strategy

  auth.serializeUser((user, done) -> done(null, user));
  auth.deserializeUser((user, done) -> done(null, user));

  robot.respond /gitlab\s+auth(?:enticate)?$/i, (msg) ->
    robot.reply msg.message, "http://192.168.33.10:8075/auth/#{msg.message.user.id}"

  robot.respond /gitlab\s+issues\s+([^\/]+)\/(.+)$/i, (msg) ->
    token = robot.brain.get "user:#{msg.message.user.id}"
    gitlab = Client.createClient("http://192.168.33.10/api/v3", token)

    gitlab.issues(msg.match[1], msg.match[2], 5).then (issues) ->
      return if issues.length is 0

      message = ""

      issues.forEach (issue) ->
        message += issueRenderer.render robot.adapterName, msg.match[1], msg.match[2], issue, 'list'
        message += "\n"

      robot.send msg.message, message

  robot.respond /gitlab\s+(?:issue\s+)?([^\/]+)\/(.+)\s*#(\d+)$/i, (msg) ->
    token = robot.brain.get "user:#{msg.message.user.id}"
    gitlab = new Client.createClient("http://192.168.33.10/api/v3", token)

    gitlab.issue(msg.match[1], msg.match[2], msg.match[3]).then (issue) ->
      robot.send(msg.message, issueRenderer.render(robot.adapterName, msg.match[1], msg.match[2], issue, 'detail')) if issue

  robot.respond /gitlab\s+(?:merge\srequests|mrs)\s+([^\/]+)\/(.+)$/i, (msg) ->
    token = robot.brain.get "user:#{msg.message.user.id}"
    gitlab = Client.createClient("http://192.168.33.10/api/v3", token)

    gitlab.merge_requests(msg.match[1], msg.match[2], 5).then (merge_requests) ->
      return if merge_requests.length is 0

      message = ""

      merge_requests.forEach (merge_request) ->
        message += mergeRequestRenderer.render robot.adapterName, msg.match[1], msg.match[2], merge_request, 'list'
        message += "\n"

      robot.send msg.message, message

  robot.respond /gitlab\s+(?:(?:merge\s+request|mr)\s+)?([^\/]+)\/(.+)\s*!(\d+)$/i, (msg) ->
    token = robot.brain.get "user:#{msg.message.user.id}"
    gitlab = Client.createClient("http://192.168.33.10/api/v3", token)

    gitlab.merge_request(msg.match[1], msg.match[2], msg.match[3]).then (merge_request) ->
      robot.send(msg.message, mergeRequestRenderer.render(robot.adapterName, msg.match[1], msg.match[2], merge_request, 'detail')) if merge_request

  robot.respond /gitlab\s+milestones\s+([^\/]+)\/(.+)$/i, (msg) ->
    token = robot.brain.get "user:#{msg.message.user.id}"
    gitlab = new Client("http://192.168.33.10/api/v3", token)

    gitlab.milestones(msg.match[1], msg.match[2], 5).then (body) ->
      robot.reply msg.message, "`#{body}`"

  robot.router.get "/auth/login", auth.authenticate "oauth2"

  robot.router.get "/auth/callback",
    auth.initialize(),
    auth.authenticate("oauth2", { failureRedirect: "/" }),
    (req, res) ->
      fs.readFile "#{__dirname}/../views/auth_callback.html", (err, content) ->
        res.setHeader 'content-type', 'text/html'
        res.end robot.webui.render content.toString(), { token: req.user }

  robot.router.post "/auth/login/done",
    auth.initialize(),
    (req, res) ->
      robot.brain.set "user:#{req.body.id}", req.body.token
      robot.brain.save

      fs.readFile "#{__dirname}/../views/auth_done.html", (err, content) ->
        res.setHeader 'content-type', 'text/html'
        res.end robot.webui.render content.toString()

  robot.router.get "/auth/:id", (req, res) ->
    fs.readFile "#{__dirname}/../views/auth.html", (err, content) ->
      res.setHeader 'content-type', 'text/html'
      res.end robot.webui.render content.toString(), { id: req.params.id }
