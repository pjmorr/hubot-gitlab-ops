module.exports = {
  render: (adapter, namespace, repository, issue, view) ->
    adapter = "generic" unless @[adapter]

    @[adapter] namespace, repository, issue, view

  generic: (namespace, repository, issue, view) ->
    "##{issue.iid} - #{issue.title}\n" +
    "http://192.168.33.10/#{namespace}/#{repository}/issues/#{issue.iid}\n"

  mattermost: (namespace, repository, issue, view) ->
    switch view
      when 'detail'
        "### [##{issue.iid} - #{issue.title}](http://192.168.33.10/#{namespace}/#{repository}/issues/#{issue.iid})\n" +
        "[_Issue_](http://192.168.33.10/#{namespace}/#{repository}/issues/#{issue.iid})_ created by _[_#{issue.author.username}_](#{issue.author.web_url})_ in _[_#{namespace}/#{repository}_](http://192.168.33.10/#{namespace}/#{repository})_ on #{issue.created_at}" +
        (if issue.assignee then " and assigned to _[_#{issue.assignee?.username}_](#{issue.assignee.web_url})" else "_") +
        "\n\n" +
        "State: [#{issue.state}](http://192.168.33.10/#{namespace}/#{repository}/issues?state=#{issue.state})\n" +
        (if issue.labels?.length then "Labels: #{issue.labels.join ","}\n" else "") +
        (if issue.milestone then "Milestone: [#{issue.milestone?.title}](http://192.168.33.10/#{namespace}/#{repository}/milestones/#{issue.milestone.iid})\n" else "") +
        "\n" +
        (if issue.description then "> #{issue.description.split("\n").map((line) -> "#{line}\n").join "> "}" else "")

      when 'list'
        "#### [##{issue.iid} - #{issue.title}](http://192.168.33.10/#{namespace}/#{repository}/issues/#{issue.iid})\n" +
        "[_Issue_](http://192.168.33.10/#{namespace}/#{repository}/issues/#{issue.iid})_ created by _[_#{issue.author.username}_](#{issue.author.web_url})_ in _[_#{namespace}/#{repository}_](http://192.168.33.10/#{namespace}/#{repository})_ on #{issue.created_at}" +
        (if issue.assignee then " and assigned to _[_#{issue.assignee?.username}_](#{issue.assignee.web_url})" else "_")
}
