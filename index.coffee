fs = require "fs"
path = require "path"
WebUI = require "./lib/webui"

module.exports = (robot, scripts) ->
  robot.webui = new WebUI robot, "#{__dirname}/static"

  scriptsPath = path.resolve __dirname, "scripts"
  fs.exists scriptsPath, (exists) ->
    if exists
      for script in fs.readdirSync(scriptsPath)
        if scripts? and '*' not in scripts
          robot.loadFile(scriptsPath, script) if script in scripts
        else
          robot.loadFile(scriptsPath, script)
