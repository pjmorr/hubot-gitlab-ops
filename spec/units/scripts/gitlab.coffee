gitlab = require "../../../scripts/gitlab"

describe "gitlab script", ->
  robot = null

  beforeEach ->
    robot =
      respond: ->
      router:
        get: ->
        post: ->
      webui:
        addMenu: ->

  describe "commands", ->
    it "should have the authenticate command", ->
      spyOn(robot, "respond");

      gitlab robot

      expect(robot.respond).toHaveBeenCalledWith /gitlab\s+auth(?:enticate)?$/i,
        jasmine.any Function

  it "should have the issues command", ->
    spyOn(robot, "respond");

    gitlab robot

    expect(robot.respond).toHaveBeenCalledWith /gitlab\s+issues\s+([^\/]+)\/(.+)$/i,
      jasmine.any Function

  it "should have the issue command", ->
    spyOn(robot, "respond");

    gitlab robot

    expect(robot.respond).toHaveBeenCalledWith /gitlab\s+(?:issue\s+)?([^\/]+)\/(.+)\s*#(\d+)$/i,
      jasmine.any Function

  it "should have the merge requests command", ->
    spyOn(robot, "respond");

    gitlab robot

    expect(robot.respond).toHaveBeenCalledWith /gitlab\s+(?:merge\srequests|mrs)\s+([^\/]+)\/(.+)$/i,
      jasmine.any Function

  it "should have the merge request command", ->
    spyOn(robot, "respond");

    gitlab robot

    expect(robot.respond).toHaveBeenCalledWith /gitlab\s+(?:(?:merge\s+request|mr)\s+)?([^\/]+)\/(.+)\s*!(\d+)$/i,
      jasmine.any Function

  describe "webui", ->
    it "should have the /auth/:id route", ->
      spyOn(robot.router, "get");

      gitlab robot

      expect(robot.router.get).toHaveBeenCalledWith "/auth/:id",
        jasmine.any Function

  it "should have the /auth/login route", ->
    spyOn(robot.router, "get");

    gitlab robot

    expect(robot.router.get).toHaveBeenCalledWith "/auth/login",
      jasmine.any Function

  it "should have the /auth/callback route", ->
    spyOn(robot.router, "get");

    gitlab robot

    expect(robot.router.get).toHaveBeenCalledWith "/auth/callback",
      jasmine.any(Function),
      jasmine.any(Function),
      jasmine.any(Function)

  it "should have the /auth/login/done route", ->
    spyOn(robot.router, "post");

    gitlab robot

    expect(robot.router.post).toHaveBeenCalledWith "/auth/login/done",
      jasmine.any(Function),
      jasmine.any(Function)

