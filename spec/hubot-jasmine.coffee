{Adapter} = require "hubot"

class Mattermost extends Adapter
  send: (envelope, strings...) ->
    console.log "foo"

  reply: (envelope, strings...) ->
    for str in strings
      @send envelope, "@#{envelope.user.name}: #{str}"

  command: (command, strings...) ->
    @send command, strings

  run: ->

exports.use = (robot) ->
  new Mattermost robot
