module.exports = {
  render: (adapter, namespace, repository, merge_request, view) ->
    adapter = "generic" unless @[adapter]

    @[adapter] namespace, repository, merge_request, view

  generic: (namespace, repository, merge_request, view) ->
    "##{merge_request.iid} - #{merge_request.title}\n" +
    "http://192.168.33.10/#{namespace}/#{repository}/merge_requests/#{merge_request.iid}\n"

  mattermost: (namespace, repository, merge_request, view) ->
    switch view
      when 'detail'
        "### [!#{merge_request.iid} - #{merge_request.title}](http://192.168.33.10/#{namespace}/#{repository}/merge_requests/#{merge_request.iid})\n" +
        "[_Merge request_](http://192.168.33.10/#{namespace}/#{repository}/merge_requests/#{merge_request.iid})_ created by _[_#{merge_request.author.username}_](#{merge_request.author.web_url})_ in _[_#{namespace}/#{repository}_](http://192.168.33.10/#{namespace}/#{repository})_ on #{merge_request.created_at}" +
        (if merge_request.assignee then " and assigned to _[_#{merge_request.assignee?.username}_](#{merge_request.assignee.web_url})" else "_") +
        "\n\n" +
        "State: [#{merge_request.state}](http://192.168.33.10/#{namespace}/#{repository}/merge_requests?state=#{merge_request.state})\n" +
        (if merge_request.labels?.length then "Labels: #{merge_request.labels.join ","}\n" else "") +
        (if merge_request.milestone then "Milestone: [#{merge_request.milestone?.title}](http://192.168.33.10/#{namespace}/#{repository}/milestones/#{merge_request.milestone.iid})\n" else "") +
        "\n" +
        (if merge_request.description then "> #{merge_request.description.split("\n").map((line) -> "#{line}\n").join "> "}" else "")

      when 'list'
        "#### [!#{merge_request.iid} - #{merge_request.title}](http://192.168.33.10/#{namespace}/#{repository}/merge_requests/#{merge_request.iid})\n" +
        "[_Merge request_](http://192.168.33.10/#{namespace}/#{repository}/merge_requests/#{merge_request.iid})_ created by _[_#{merge_request.author.username}_](#{merge_request.author.web_url})_ in _[_#{namespace}/#{repository}_](http://192.168.33.10/#{namespace}/#{repository})_ on #{merge_request.created_at}" +
        (if merge_request.assignee then " and assigned to _[_#{merge_request.assignee?.username}_](#{merge_request.assignee.web_url})" else "_")
}
